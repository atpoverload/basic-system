﻿using System;

namespace BasicSystem.Adventure
{
    [Serializable]
    public abstract class Action : BasicSystem.Action
    {
        #region Visualization
        public string Name { get; private set; }
        protected Action(string name) { Name = name; }
        #endregion

        #region Interaction
        public abstract void Interact(Object source, Object target);
        #endregion
    }
}