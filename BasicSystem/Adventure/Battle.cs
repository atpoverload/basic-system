﻿using System.Collections.Generic;

namespace BasicSystem.Adventure
{
    public abstract class Battle : Event
    {
        public Character Current { get { return OrderedCharacters.Peek(); } }

        protected Queue<Character> OrderedCharacters = new Queue<Character>();
        protected IEnumerable<Character> Characters;

        protected Battle(IEnumerable<Character> characters) { Characters = characters; Tick(); }

        public abstract void Tick();
    }
}