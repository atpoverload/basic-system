﻿using System;
using System.Collections.Generic;

namespace BasicSystem.Adventure
{
    [Serializable]
    public abstract class Character : BasicSystem.Character
    {
        #region Visualization
        public string Name { get; private set; }
        public Character(string name) { Name = name; }
        #endregion

        #region Interaction
        private List<BasicSystem.Action> actions = new List<BasicSystem.Action>();
        public IEnumerable<BasicSystem.Action> Actions { get { return actions; } }

        public bool Add(BasicSystem.Action action)
        {
            if (action is Action)
            {
                actions.Add(action);
                return true;
            }

            return false;
        }

        public bool Remove(BasicSystem.Action action)
        {
            if (action is Action)
                return actions.Remove(action);

            return false;
        }
        #endregion
    }
}