﻿namespace BasicSystem
{
    #region Entities
    public interface Object { }

    public interface Action : Interactor<Object, Object> { }
    #endregion

    #region Structure
    public interface Acquisitor<T>
    {
        bool Add(T item);
        bool Remove(T item);
    }

    public interface Interactor<S, T>
    {
        void Interact(S source, T target);
        //void Interact(S source, IEnumerable<T> targets);
    }

    public interface Frame { void Read(object reader); }
    #endregion

    public interface Character : Object, Acquisitor<Action> { string Name { get; } }

    public interface Player { }

    public interface Event { void Tick(); }
}