﻿using System.Collections.Generic;

namespace BasicSystem.Adventure
{
    public enum Direction { Right = 0, Up = 1, Left = 2, Down = 3 };

    public class Node<K>
    {
        private IDictionary<K, Node<K>> children = new Dictionary<K, Node<K>>();

        protected Node<K> Get(K key)
        {
            if (!children.ContainsKey(key))
                return null;

            return children[key];
        }

        protected void Set(K key, Node<K> value)
        {
            if (!children.ContainsKey(key))
                children.Add(key, null);

            children[key] = value;
        }
    }

    public static class Operations
    {
        public static Direction Invert(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Down;
                case Direction.Down:
                    return Direction.Up;
                case Direction.Left:
                    return Direction.Right;
                default:
                    return Direction.Left;
            }
        }

        public static Direction RotateLeft(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Left;
                case Direction.Down:
                    return Direction.Right;
                case Direction.Left:
                    return Direction.Down;
                default:
                    return Direction.Up;
            }
        }

        public static Direction RotateRight(this Direction direction)
        {
            switch (direction)
            {
                case Direction.Up:
                    return Direction.Right;
                case Direction.Down:
                    return Direction.Left;
                case Direction.Left:
                    return Direction.Up;
                default:
                    return Direction.Down;
            }
        }
    }

    /*
    public class Boundaries
    {
        private IEnumerable<Polygon> boundaries;

        public bool Contains(Point point)
        {
            foreach (Polygon bound in boundaries)
                if (bound.Contains(point))
                    return true;

            return false;
        }
        public Boundaries(IEnumerable<Polygon> boundaries) { this.boundaries = boundaries; }
    }

    public interface Room
    {
        IEnumerable<Prop> Inhabitants { get; }
        Boundaries Boundaries { get; }
        Room this[Direction direction] { get; }
    }

    public class Map : Room
    {
        public static int Height = 5;
        public static int Width = 5;

        private int current = 0;

        private int Rooms = 0;
        private IDictionary<int, IDictionary<Direction, int>> Neighbors = new Dictionary<int, IDictionary<Direction, int>>();
        private IDictionary<int, Boundaries> Walls = new Dictionary<int, Boundaries>();
        private IDictionary<int, IEnumerable<Prop>> Props = new Dictionary<int, IEnumerable<Prop>>();

        public Map() { }
        internal Map(int current, Map map)
        {
            this.current = current;
            Rooms = map.Rooms;
            Neighbors = map.Neighbors;
            Walls = map.Walls;
            Props = map.Props;
        }

        internal void Add()
        {
            Neighbors.Add(Rooms, new Dictionary<Direction, int>());
            foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                Neighbors[Rooms].Add(direction, -1);
            Walls.Add(Rooms, null);
            Props.Add(Rooms, null);
            Rooms++;
        }

        internal void Add(Prop inhabitant)
        {
            Neighbors.Add(Rooms, new Dictionary<Direction, int>());
            foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                Neighbors[Rooms].Add(direction, -1);
            Walls.Add(Rooms, null);
            Props.Add(Rooms, new Prop[] { inhabitant });
            Rooms++;
        }

        internal void Add(IEnumerable<Prop> inhabitants)
        {
            Neighbors.Add(Rooms, new Dictionary<Direction, int>());
            foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                Neighbors[Rooms].Add(direction, -1);
            Walls.Add(Rooms, null);
            Props.Add(Rooms, inhabitants);
            Rooms++;
        }
        internal void Join(int first, int second, Direction direction)
        {
            Neighbors[first][direction] = second;
            Neighbors[second][direction.Invert()] = first;
        }
        internal void PopulateWalls()
        {
            for (int room = 0; room < Rooms; room++)
            {
                IList<Polygon> walls = new List<Polygon>();
                foreach (Direction direction in Enum.GetValues(typeof(Direction)))
                    if (Neighbors[room][direction] == -1)
                        walls.Add(Game.Master.LineWalls[direction]);
                    else
                    {
                        if (Neighbors[Neighbors[room][direction]][direction.RotateLeft()] == -1)
                            walls.Add(Game.Master.BlockWalls[direction][direction.RotateLeft()]);
                        if (Neighbors[Neighbors[room][direction]][direction.RotateRight()] == -1)
                            walls.Add(Game.Master.BlockWalls[direction][direction.RotateRight()]);
                    }
                Walls[room] = new Boundaries(walls);
            }
        }

        Room Room.this[Direction direction]
        {
            get
            {
                if (Neighbors[current][direction] != -1)
                    return new Map(Neighbors[current][direction], this);
                else
                    return this;
            }
        }
        public Boundaries Boundaries { get { return Walls[current]; } }
        public IEnumerable<Prop> Inhabitants { get { return Props[current]; } }
    }

    /*
    public class Map
    {
        private Room[,] Rooms { get; set; }
        private IDictionary<Room, IDictionary<Direction, bool>> Neighbors { get; set; }
        public Room this[int x, int y] { get { return Rooms[x, y]; } }

        public Map() { }
    }

    public class Room : Node<Direction>
    {
        public readonly int width;
        public readonly int height;

        public Room this[Direction direction]
        {
            get { return Get(direction) as Room; }
            set { Set(direction, value); value.Set(direction.Invert(), this); }
        }

        public Room(int width, int height) { this.width = width; this.height = height; }

        public enum Type { Empty, Monster, Stairs }
        public bool[,] Positions { get; private set; }
        public object Inhabitant { get; set; }
        public void Populate(Type type)
        {
            Inhabitant = new Player();
            Positions = new bool[width, height];

            for (int i = 0; i < width; i++)
                for (int j = 0; j < height; j++)
                    if (!Positions[i, j])
                        if (this[Direction.Right] == null && i == width - 1)
                            Positions[i, j] = true;
                        else if (this[Direction.Up] == null && j == height - 1)
                            Positions[i, j] = true;
                        else if (this[Direction.Left] == null && i == 0)
                            Positions[i, j] = true;
                        else if (this[Direction.Down] == null && j == 0)
                            Positions[i, j] = true;
                        else
                            Positions[i, j] = false;

            switch (type)
            {
                case Type.Stairs:
                    Inhabitant = new Player(Premade.Monsters.Stairs());
                    break;
                case Type.Monster:
                    Inhabitant = new Player(Premade.Monsters.Ooze(1), Premade.Monsters.Ooze(2), Premade.Monsters.Ooze(3), Premade.Monsters.Ooze(4));
                    break;
            }
        }
    }*/
}