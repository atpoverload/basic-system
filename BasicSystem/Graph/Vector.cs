﻿using System;

namespace BasicSystem.Graph
{
    public struct Vector
    {
        #region Implementation
        public float X { get; private set; }
        public float Y { get; private set; }
        public float Z { get; private set; }

        public float Magnitude { get { return (float)Math.Sqrt(this * this); } }
        public Vector Normal { get { return this / Magnitude; } }

        public Vector(float x, float y) { X = x; Y = y; Z = 0; }
        public Vector(float x, float y, float z) { X = x; Y = y; Z = z; }

        public override bool Equals(object obj) { return obj is Vector && GetHashCode() == obj.GetHashCode(); }
        public override int GetHashCode() { return ((17 + X.GetHashCode()) * 31 + Y.GetHashCode()); }
        public override string ToString() { return X + "," + Y + "," + Z; }
        #endregion
   
        #region Operators
        public static Vector operator +(Vector first, Vector second) { return new Vector(first.X + second.X, first.Y + second.Y, first.Z + second.Z); }
        public static Vector operator -(Vector first, Vector second) { return new Vector(first.X - second.X, first.Y - second.Y, first.Z - second.Z); }
        public static Vector operator *(Vector vector, float scalar) { return new Vector(vector.X * scalar, vector.Y * scalar, vector.Z * scalar); }
        public static Vector operator /(Vector vector, float scalar) { return new Vector(vector.X / scalar, vector.Y / scalar, vector.Z / scalar); }
        public static float operator *(Vector first, Vector second) { return first.X * second.X + first.Y * second.Y + first.Z * second.Z; }
        public static Vector operator ^(Vector first, Vector second)
        {
            float x = first.Y * second.Z - first.Z * second.X;
            float y = first.Z * second.X - first.X * second.Z;
            float z = first.X * second.Y - first.Y * second.X;
            return new Vector(x, y, z);
        }

        public static bool operator ==(Vector first, Vector second) { return first.Equals(second); }
        public static bool operator !=(Vector first, Vector second) { return !(first == second); }
        #endregion

        #region Unit Vectors
        public static Vector Zero = new Vector(0, 0, 0);

        public static Vector Left = new Vector(-1, 0, 0);
        public static Vector Right = new Vector(1, 0, 0);

        public static Vector Up = new Vector(0, 1, 0);
        public static Vector Down = new Vector(0, -1, 0);

        public static Vector Forward = new Vector(0, 0, 1);
        public static Vector Backward = new Vector(0, 0, -1);
        #endregion
    }
}