﻿namespace BasicSystem.Graph
{
    public struct Point
    {
        #region Implementation
        public float X { get; private set; }
        public float Y { get; private set; }

        public Point(float x, float y) { X = x; Y = y; }

        public override bool Equals(object obj) { return obj is Point && GetHashCode() == obj.GetHashCode(); }
        public override int GetHashCode() { return ((17 + X.GetHashCode()) * 31 + Y.GetHashCode()); }
        public override string ToString() { return X + "," + Y; }
        #endregion

        #region Operators
        public static Point operator +(Point point, Vector vector) { return new Point(point.X + vector.X, point.Y + vector.Y); }
        public static Point operator -(Point point, Vector vector) { return new Point(point.X - vector.X, point.Y - vector.Y); }

        public static Vector operator -(Point first, Point second) { return new Vector(first.X - second.X, first.Y - second.Y); }

        public static bool operator ==(Point first, Point second) { return first.Equals(second); }
        public static bool operator !=(Point first, Point second) { return !(first == second); }
        #endregion
    }
}