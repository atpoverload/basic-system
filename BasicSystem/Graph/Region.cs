﻿using System.Collections.Generic;

namespace BasicSystem.Graph
{
    public struct Region
    {
        private IDictionary<Vector, Object> occupiedPositions;
        private IDictionary<Object, Vector> occupiedPositionsDual;

        public Object this[int i, int j] { get { return occupiedPositions[new Vector(i, j)]; } }
        public Vector this[Object thing] { get { return occupiedPositionsDual[thing]; } }

        public Region(int sizex)
        {
            occupiedPositions = new Dictionary<Vector, Object>();
            occupiedPositionsDual = new Dictionary<Object, Vector>();
        }

        public bool Add(Object thing, Vector position)
        {
            if (!occupiedPositions.ContainsKey(position))
            {
                occupiedPositions.Add(position, thing);
                occupiedPositionsDual.Add(thing, position);
                return true;
            }

            return false;
        }

        public bool Remove(Object thing, Vector position)
        {
            if (occupiedPositions.ContainsKey(position))
            {
                occupiedPositions.Remove(position);
                occupiedPositionsDual.Remove(thing);
                return true;
            }

            return false;
        }

        public bool Move(Object thing, Vector position)
        {
            if (occupiedPositionsDual.ContainsKey(thing))
                if (Add(thing, occupiedPositionsDual[thing] + position))
                    return Remove(thing, occupiedPositionsDual[thing]);

            return false;
        }
    }
}