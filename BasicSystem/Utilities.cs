﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace BasicSystem
{
    public static class Utilities
    {
        public static int Bound(int min, int value, int max) { return Math.Max(0, Math.Min(value, 100)); }

        public static IEnumerable<T> AllEnumValues<T>() { return Enum.GetValues(typeof(T)).Cast<T>(); }

        private static Random Rand = new Random();

        public static T RandomElement<T>(this IEnumerable<T> enumerable) { return enumerable.Skip(Rand.Next(enumerable.Count())).FirstOrDefault(); }

        public static T2 RandomEntry<T1, T2>(this IDictionary<T1, T2> dictionary) { return dictionary.RandomElement().Value; }

        public static T RandomEnumValue<T>() { return Enum.GetValues(typeof(T)).Cast<T>().RandomElement(); }
    }
}