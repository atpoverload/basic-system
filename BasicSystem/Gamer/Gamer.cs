﻿using System;

namespace BasicSystem.Gamer
{
    [Serializable]
    public abstract class Gamer : Player
    {
        private static uint lastID = 0;

        protected uint id;
        public uint ID { get { return id; } }

        protected Gamer() { id = ++lastID; }
    }

    [Serializable]
    public abstract class GameMaster : Gamer
    {
        protected GameMaster() { id = uint.MinValue; }

        public abstract void Create();
        public abstract void Destroy();
    }
}